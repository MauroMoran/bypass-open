﻿using Renci.SshNet;
using Renci.SshNet.Common;
using Renci.SshNet.Sftp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace iCloudBypass_ShiftKey
{
    public class Form1 : Form
    {
        private string path = "";
        private string uid = "";
        private string host = "127.0.0.1";
        private string user = "root";
        private string pass = "alpine";
        private IContainer components = (IContainer)null;
        private Process proc;
        private TextBox txtlog;
        private Button button1;
        private Button button2;
        private Button button3;
        private Label label3;
        private Label label4;
        private Label label5;
        private Button button4;
        private Button button7;
        private Button btCYDIA;
        private Button btCHECKRA1N;
        private Button button6;
        private Button button8;
        private Button button9;
        private Button button5;

        public Form1()
        {
            this.InitializeComponent();
            this.path = Directory.GetCurrentDirectory();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.button1.Enabled = false;
            this.button2.Enabled = false;
            this.txtlog.Text = "";
            this.txtlog.Text += "obteniendo UID... \r\n";
            if (this.getDeviceUID())
            {
                this.txtlog.Text += "Creando la conexion ssh... \r\n";
                this.createCon();
                this.txtlog.Text += "Conectando con el dispositivo \r\n";
                this.sshCommand();
            }
            this.button1.Enabled = true;
            this.button2.Enabled = true;
        }

        public bool getDeviceUID()
        {
            bool flag = false;
            Process process = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = this.path + "\\library\\idevice_id.exe",
                    Arguments = "-l",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            process.Start();
            while (!process.StandardOutput.EndOfStream)
            {
                string str = process.StandardOutput.ReadLine();
                if (str.Contains("Unable"))
                {
                    int num = (int)MessageBox.Show("Por favor verifique tener instalado itunes.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                else
                {
                    TextBox txtlog = this.txtlog;
                    txtlog.Text = txtlog.Text + "UID: " + str + "\r\n";
                    this.uid = str;
                    flag = true;
                    Microsoft.AppCenter.Analytics.Analytics.TrackEvent(this.uid, (IDictionary<string, string>)null);
                }
            }
            if (!flag)
            {
                int num1 = (int)MessageBox.Show("iPhone no conectado... Por favor veifique el cable usb y tener instalado itunes.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return flag;
        }

        public void createCon()
        {
            this.proc = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = this.path + "\\library\\iproxy.exe",
                    Arguments = "22 44 " + this.uid,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            this.proc.Start();
            this.txtlog.Text += "Conexion creada, en espera...\r\n";
        }

        public async void sshCommand()
        {
            SshClient sshclient = new SshClient(this.host, this.user, this.pass);
            try
            {
                this.txtlog.Text += "Conectado, Ejecutando comandos \r\n";
                sshclient.Connect();
                SshCommand mount = sshclient.CreateCommand("mount -o rw,union,update /");
                SshCommand echo = sshclient.CreateCommand("echo \" >> /.mount_rw");
                SshCommand mv = sshclient.CreateCommand("mv /Applications/Setup.app /Applications/Setup.app.crae");
                SshCommand uicache = sshclient.CreateCommand("uicache --all");
                SshCommand killall = sshclient.CreateCommand("killall backboardd");
                IAsyncResult asynch = mount.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(2000);
                string result = mount.EndExecute(asynch);
                asynch = echo.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(2000);
                result = echo.EndExecute(asynch);
                asynch = mv.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(2000);
                result = mv.EndExecute(asynch);
                asynch = uicache.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(5000);
                result = uicache.EndExecute(asynch);
                asynch = killall.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(2000);
                result = killall.EndExecute(asynch);
                sshclient.Disconnect();
                this.txtlog.Text += "Finalizado. \r\n";
                this.stopProxi();
                Microsoft.AppCenter.Analytics.Analytics.TrackEvent("Bypass completo: " + this.uid, (IDictionary<string, string>)null);
                mount = (SshCommand)null;
                echo = (SshCommand)null;
                mv = (SshCommand)null;
                uicache = (SshCommand)null;
                killall = (SshCommand)null;
                asynch = (IAsyncResult)null;
                result = (string)null;
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(ex.Message + " : " + this.uid, (IDictionary<string, string>)null);
                if (!ex.Message.Contains("SSH protocol identification"))
                    return;
                int num = (int)MessageBox.Show("Verifique el estado de su JailBreak", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        public void fixAppStore()
        {
            SshClient sshClient = new SshClient(this.host, this.user, this.pass);
            try
            {
                this.txtlog.Text += "Ejecutando comandos \r\n";
                sshClient.Connect();
                SshCommand command = sshClient.CreateCommand("mv /var/mobile/Library/Preferences/com.apple.purplebuddy.plist /var/mobile/Library/Preferences/com.apple.purplebuddy.plist.old");
                IAsyncResult asyncResult = command.BeginExecute();
                while (!asyncResult.IsCompleted)
                    Thread.Sleep(2000);
                command.EndExecute(asyncResult);
                sshClient.Disconnect();
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(ex.Message + " : " + this.uid, (IDictionary<string, string>)null);
                if (ex.Message.Contains("SSH protocol identification"))
                {
                    int num = (int)MessageBox.Show("Verifique el etado de su JailBreak", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            ScpClient scpClient = new ScpClient(this.host, this.user, this.pass);
            try
            {
                scpClient.Connect();
                scpClient.Upload(new FileInfo(this.path + "\\library\\com.apple.purplebuddy.plist"), "/var/mobile/Library/Preferences/com.apple.purplebuddy.plist");
                scpClient.Disconnect();
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(ex.Message + " : " + this.uid, (IDictionary<string, string>)null);
                if (ex.Message.Contains("SSH protocol identification"))
                {
                    int num = (int)MessageBox.Show("Verifique el estado de su JailBreak", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    Process.Start("https://youtu.be/DlUuJt2Xhuw");
                }
            }
            this.txtlog.Text += "AppStore parcheado, Reinicie su dispositivo \r\n";
            this.stopProxi();
        }

        public void Bypass13()
        {
            SshClient sshClient1 = new SshClient(this.host, this.user, this.pass);
            try
            {
                this.txtlog.Text += "Ejecutando comandos \r\n";
                sshClient1.Connect();
                SshCommand command1 = sshClient1.CreateCommand("chmod 0000 /Applications/Setup.app/Setup");
                SshCommand command2 = sshClient1.CreateCommand("mount -o rw,union,update /");
                SshCommand command3 = sshClient1.CreateCommand("echo \" >> /.mount_rw");
                IAsyncResult asyncResult1 = command2.BeginExecute();
                while (!asyncResult1.IsCompleted)
                    Thread.Sleep(1000);
                string str = command2.EndExecute(asyncResult1);
                IAsyncResult asyncResult2 = command3.BeginExecute();
                while (!asyncResult2.IsCompleted)
                    Thread.Sleep(1000);
                str = command3.EndExecute(asyncResult2);
                IAsyncResult asyncResult3 = command1.BeginExecute();
                while (!asyncResult3.IsCompleted)
                    Thread.Sleep(1000);
                str = command1.EndExecute(asyncResult3);
                sshClient1.Disconnect();
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(ex.Message + " : " + this.uid, (IDictionary<string, string>)null);
                if (ex.Message.Contains("SSH protocol identification"))
                {
                    int num = (int)MessageBox.Show("Verifique el etado de su JailBreak", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            ScpClient scpClient = new ScpClient(this.host, this.user, this.pass);
            try
            {
                scpClient.Connect();
                scpClient.Upload(new FileInfo(this.path + "\\library\\PreferenceFix"), "/Applications/Preferences.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\AppStoreFix"), "/Applications/AppStore.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\CameraFix"), "/Applications/Camera.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\SafariFix"), "/Applications/MobileSafari.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\AMA"), "/Applications/ActivityMessagesApp.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\AAD"), "/Applications/AccountAuthenticationDialog.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\AS"), "/Applications/AnimojiStickers.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\ATR"), "/Applications/Apple TV Remote.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\ASOS"), "/Applications/AppSSOUIService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\APUI"), "/Applications/AskPermissionUI.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\AKUS"), "/Applications/AuthKitUIService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\AVS"), "/Applications/AXUIViewService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\BS"), "/Applications/BarcodeScanner.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\BCVS"), "/Applications/BusinessChatViewService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\BEW"), "/Applications/BusinessExtensionsWrapper.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\CPS"), "/Applications/CarPlaySettings.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\CPSS"), "/Applications/CarPlaySplashScreen.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\CCVS"), "/Applications/CompassCalibrationViewService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\CCSA"), "/Applications/CTCarrierSpaceAuth.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\CNUS"), "/Applications/CTNotifyUIService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\DA"), "/Applications/DataActivation.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\DDAS"), "/Applications/DDActionsService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\DEA"), "/Applications/DemoApp.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\Diag"), "/Applications/Diagnostics.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\DServ"), "/Applications/DiagnosticsService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\DNB"), "/Applications/DNDBuddy.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\FAM"), "/Applications/Family.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\FBAI"), "/Applications/Feedback Assistant iOS.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\FT"), "/Applications/FieldTest.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\FM"), "/Applications/FindMy.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\FIVS"), "/Applications/FontInstallViewService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\FTMI"), "/Applications/FTMInternal-4.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\FCES"), "/Applications/FunCameraEmojiStickers.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\FCT"), "/Applications/FunCameraText.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\GCUS"), "/Applications/GameCenterUIService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\HI"), "/Applications/HashtagImages.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\H"), "/Applications/Health.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\HPS"), "/Applications/HealthPrivacyService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\HUS"), "/Applications/HomeUIService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\iAdOp"), "/Applications/iAdOptOut.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\iCloud"), "/Applications/iCloud.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\IMAVS"), "/Applications/iMessageAppsViewService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\ICS"), "/Applications/InCallService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\M"), "/Applications/Magnifier.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\MCS"), "/Applications/MailCompositionService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\MSS"), "/Applications/MobileSlideShow.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\MSMS"), "/Applications/MobileSMS.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\MT"), "/Applications/MobileTimer.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\MUIS"), "/Applications/MusicUIService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\PB"), "/Applications/Passbook.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\PUS"), "/Applications/PassbookUIService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\PVS"), "/Applications/PhotosViewService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\Pc"), "/Applications/Print Center.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\SVS"), "/Applications/SafariViewService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\SSVS"), "/Applications/ScreenSharingViewService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\SCSS"), "/Applications/ScreenshotServicesService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\STU"), "/Applications/ScreenTimeUnlock.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\SWCVS"), "/Applications/SharedWebCredentialViewService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\SDC"), "/Applications/Sidecar.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\SSUS"), "/Applications/SIMSetupUIService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\Siri"), "/Applications/Siri.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\SUUS"), "/Applications/SoftwareUpdateUIService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\SPC"), "/Applications/SPNFCURL.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\SLI"), "/Applications/Spotlight.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\SDVS"), "/Applications/StoreDemoViewService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\SKUS"), "/Applications/StoreKitUIService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\TAVS"), "/Applications/TVAccessViewService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\VSAVS"), "/Applications/VideoSubscriberAccountViewService.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\Wb"), "/Applications/Web.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\WCAUI"), "/Applications/WebContentAnalysisUI.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\WS"), "/Applications/WebSheet.app/Info.plist");
                scpClient.Upload(new FileInfo(this.path + "\\library\\Application.tar"), "/private/var/containers/Bundle/Application.tar");
                scpClient.Disconnect();
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(ex.Message + " : " + this.uid, (IDictionary<string, string>)null);
                if (ex.Message.Contains("SSH protocol identification"))
                {
                    int num = (int)MessageBox.Show("Verifique el estado de su JailBreak", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    Process.Start("https://youtu.be/DlUuJt2Xhuw");
                }
            }
            SshClient sshClient2 = new SshClient(this.host, this.user, this.pass);
            try
            {
                this.txtlog.Text += "Ejecutando comandos \r\n";
                sshClient2.Connect();
                SshCommand command1 = sshClient2.CreateCommand("rm -R /private/var/containers/Bundle/Application/");
                SshCommand command2 = sshClient2.CreateCommand("tar -xvf /private/var/containers/Bundle/Application.tar -C /private/var/containers/Bundle/");
                SshCommand command3 = sshClient2.CreateCommand("rm /private/var/containers/Bundle/Application.tar");
                SshCommand command4 = sshClient2.CreateCommand("uicache -a");
                SshCommand command5 = sshClient2.CreateCommand("killall backboardd");
                SshCommand command6 = sshClient2.CreateCommand("/Applications/PreBoard.app/PreBoard &");
                SshCommand command7 = sshClient2.CreateCommand("killall PreBoard");
                IAsyncResult asyncResult1 = command1.BeginExecute();
                while (!asyncResult1.IsCompleted)
                    Thread.Sleep(2000);
                string str = command1.EndExecute(asyncResult1);
                IAsyncResult asyncResult2 = command2.BeginExecute();
                while (!asyncResult2.IsCompleted)
                    Thread.Sleep(5000);
                str = command2.EndExecute(asyncResult2);
                IAsyncResult asyncResult3 = command3.BeginExecute();
                while (!asyncResult3.IsCompleted)
                    Thread.Sleep(23000);
                str = command3.EndExecute(asyncResult3);
                IAsyncResult asyncResult4 = command4.BeginExecute();
                while (!asyncResult4.IsCompleted)
                    Thread.Sleep(3000);
                str = command4.EndExecute(asyncResult4);
                IAsyncResult asyncResult5 = command5.BeginExecute();
                while (!asyncResult5.IsCompleted)
                    Thread.Sleep(5000);
                str = command5.EndExecute(asyncResult5);
                IAsyncResult asyncResult6 = command6.BeginExecute();
                while (!asyncResult6.IsCompleted)
                    Thread.Sleep(5000);
                str = command6.EndExecute(asyncResult6);
                IAsyncResult asyncResult7 = command7.BeginExecute();
                while (!asyncResult7.IsCompleted)
                    Thread.Sleep(2000);
                str = command7.EndExecute(asyncResult7);
                sshClient1.Disconnect();
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(ex.Message + " : " + this.uid, (IDictionary<string, string>)null);
                if (ex.Message.Contains("SSH protocol identification"))
                {
                    int num = (int)MessageBox.Show("Verifique el etado de su JailBreak", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            this.stopProxi();
        }

        public void CheckRa1n()
        {
            ScpClient scpClient = new ScpClient(this.host, this.user, this.pass);
            try
            {
                scpClient.Connect();
                scpClient.Upload(new FileInfo(this.path + "\\library\\loader2.app.tar"), "/Applications/loader2.app.tar");
                scpClient.Disconnect();
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(ex.Message + " : " + this.uid, (IDictionary<string, string>)null);
                if (ex.Message.Contains("SSH protocol identification"))
                {
                    int num = (int)MessageBox.Show("Verifique el estado de su JailBreak", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    Process.Start("https://youtu.be/DlUuJt2Xhuw");
                }
            }
            SshClient sshClient = new SshClient(this.host, this.user, this.pass);
            try
            {
                this.txtlog.Text += "Ejecutando comandos \r\n";
                sshClient.Connect();
                SshCommand command1 = sshClient.CreateCommand("mount -o rw,union,update /");
                SshCommand command2 = sshClient.CreateCommand("cd /Applications/");
                SshCommand command3 = sshClient.CreateCommand("tar -xvf /Applications/loader2.app.tar -C /Applications");
                SshCommand command4 = sshClient.CreateCommand("chmod -R 0777 /Applications/loader2.app/");
                SshCommand command5 = sshClient.CreateCommand("uicache -a");
                SshCommand command6 = sshClient.CreateCommand("killall backboardd");
                SshCommand command7 = sshClient.CreateCommand("/Applications/PreBoard.app/PreBoard &");
                SshCommand command8 = sshClient.CreateCommand("killall PreBoard");
                IAsyncResult asyncResult1 = command1.BeginExecute();
                while (!asyncResult1.IsCompleted)
                    Thread.Sleep(2000);
                string str = command1.EndExecute(asyncResult1);
                IAsyncResult asyncResult2 = command2.BeginExecute();
                while (!asyncResult2.IsCompleted)
                    Thread.Sleep(2000);
                str = command2.EndExecute(asyncResult2);
                IAsyncResult asyncResult3 = command3.BeginExecute();
                while (!asyncResult3.IsCompleted)
                    Thread.Sleep(2000);
                str = command3.EndExecute(asyncResult3);
                IAsyncResult asyncResult4 = command4.BeginExecute();
                while (!asyncResult4.IsCompleted)
                    Thread.Sleep(2000);
                str = command4.EndExecute(asyncResult4);
                IAsyncResult asyncResult5 = command5.BeginExecute();
                while (!asyncResult5.IsCompleted)
                    Thread.Sleep(1000);
                str = command5.EndExecute(asyncResult5);
                IAsyncResult asyncResult6 = command6.BeginExecute();
                while (!asyncResult6.IsCompleted)
                    Thread.Sleep(5000);
                str = command6.EndExecute(asyncResult6);
                IAsyncResult asyncResult7 = command7.BeginExecute();
                while (!asyncResult7.IsCompleted)
                    Thread.Sleep(5000);
                str = command7.EndExecute(asyncResult7);
                IAsyncResult asyncResult8 = command8.BeginExecute();
                while (!asyncResult8.IsCompleted)
                    Thread.Sleep(2000);
                str = command8.EndExecute(asyncResult8);
                sshClient.Disconnect();
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(ex.Message + " : " + this.uid, (IDictionary<string, string>)null);
                if (!ex.Message.Contains("SSH protocol identification"))
                    return;
                int num = (int)MessageBox.Show("Verifique el etado de su JailBreak", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        public void cydiaFix()
        {
            ScpClient scpClient = new ScpClient(this.host, this.user, this.pass);
            try
            {
                scpClient.Connect();
                scpClient.Upload(new FileInfo(this.path + "\\library\\CydiaFix"), "/Applications/Cydia.app/Info.plist");
                scpClient.Disconnect();
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(ex.Message + " : " + this.uid, (IDictionary<string, string>)null);
                if (ex.Message.Contains("SSH protocol identification"))
                {
                    int num = (int)MessageBox.Show("Verifique el estado de su JailBreak", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    Process.Start("https://youtu.be/DlUuJt2Xhuw");
                }
            }
            SshClient sshClient = new SshClient(this.host, this.user, this.pass);
            try
            {
                this.txtlog.Text += "Ejecutando comandos \r\n";
                sshClient.Connect();
                SshCommand command1 = sshClient.CreateCommand("mount -o rw,union,update /");
                SshCommand command2 = sshClient.CreateCommand("uicache -a");
                SshCommand command3 = sshClient.CreateCommand("killall backboardd");
                SshCommand command4 = sshClient.CreateCommand("/Applications/PreBoard.app/PreBoard &");
                SshCommand command5 = sshClient.CreateCommand("killall PreBoard");
                IAsyncResult asyncResult1 = command1.BeginExecute();
                while (!asyncResult1.IsCompleted)
                    Thread.Sleep(2000);
                string str = command1.EndExecute(asyncResult1);
                IAsyncResult asyncResult2 = command2.BeginExecute();
                while (!asyncResult2.IsCompleted)
                    Thread.Sleep(1000);
                str = command2.EndExecute(asyncResult2);
                IAsyncResult asyncResult3 = command3.BeginExecute();
                while (!asyncResult3.IsCompleted)
                    Thread.Sleep(5000);
                str = command3.EndExecute(asyncResult3);
                IAsyncResult asyncResult4 = command4.BeginExecute();
                while (!asyncResult4.IsCompleted)
                    Thread.Sleep(2000);
                str = command4.EndExecute(asyncResult4);
                IAsyncResult asyncResult5 = command5.BeginExecute();
                while (!asyncResult5.IsCompleted)
                    Thread.Sleep(3000);
                str = command5.EndExecute(asyncResult5);
                sshClient.Disconnect();
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(ex.Message + " : " + this.uid, (IDictionary<string, string>)null);
                if (!ex.Message.Contains("SSH protocol identification"))
                    return;
                int num = (int)MessageBox.Show("Verifique el etado de su JailBreak", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        public void home()
        {
            SshClient sshClient = new SshClient(this.host, this.user, this.pass);
            try
            {
                this.txtlog.Text += "Ejecutando comandos \r\n";
                sshClient.Connect();
                SshCommand command1 = sshClient.CreateCommand("mount -o rw,union,update /");
                SshCommand command2 = sshClient.CreateCommand("chmod 0000 /Applications/Setup.app/Setup");
                SshCommand command3 = sshClient.CreateCommand("uicache -a");
                SshCommand command4 = sshClient.CreateCommand("killall backboardd");
                SshCommand command5 = sshClient.CreateCommand("/Applications/PreBoard.app/PreBoard &");
                SshCommand command6 = sshClient.CreateCommand("killall PreBoard");
                IAsyncResult asyncResult1 = command1.BeginExecute();
                while (!asyncResult1.IsCompleted)
                    Thread.Sleep(1000);
                string str = command1.EndExecute(asyncResult1);
                IAsyncResult asyncResult2 = command2.BeginExecute();
                while (!asyncResult2.IsCompleted)
                    Thread.Sleep(1000);
                str = command2.EndExecute(asyncResult2);
                IAsyncResult asyncResult3 = command3.BeginExecute();
                while (!asyncResult3.IsCompleted)
                    Thread.Sleep(10000);
                str = command3.EndExecute(asyncResult3);
                IAsyncResult asyncResult4 = command4.BeginExecute();
                while (!asyncResult4.IsCompleted)
                    Thread.Sleep(10000);
                str = command4.EndExecute(asyncResult4);
                IAsyncResult asyncResult5 = command5.BeginExecute();
                while (!asyncResult5.IsCompleted)
                    Thread.Sleep(2000);
                str = command5.EndExecute(asyncResult5);
                IAsyncResult asyncResult6 = command6.BeginExecute();
                while (!asyncResult6.IsCompleted)
                    Thread.Sleep(2000);
                str = command6.EndExecute(asyncResult6);
                sshClient.Disconnect();
            }
            catch (Exception ex)
            {
                Microsoft.AppCenter.Analytics.Analytics.TrackEvent(ex.Message + " : " + this.uid, (IDictionary<string, string>)null);
                if (ex.Message.Contains("SSH protocol identification"))
                {
                    int num = (int)MessageBox.Show("Verifique que el puerto 22 no este ocupado por otro programa.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            this.txtlog.Text = "Respring completado";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.button1.Enabled = false;
            this.button2.Enabled = false;
            this.txtlog.Text = "";
            this.txtlog.Text += "Iniciando proceso... \r\n";
            if (this.getDeviceUID())
            {
                this.txtlog.Text += "Creando la conexion ssh... \r\n";
                this.createCon();
                this.txtlog.Text += "Conectando con el dispositivo \r\n";
                this.fixAppStore();
            }
            this.button1.Enabled = true;
            this.button2.Enabled = true;
        }

        public void stopProxi()
        {
            this.proc.Kill();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pcYoutubeLabel_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.youtube.com/user/osekom1");
        }

        private void pcYoutube_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.youtube.com/user/osekom1");
        }

        private void pcTwitter_Click(object sender, EventArgs e)
        {
            Process.Start("https://twitter.com/LeoManrique7");
        }

        private void pcTwitterLabel_Click(object sender, EventArgs e)
        {
            Process.Start("https://twitter.com/LeoManrique7");
        }

        private void pcInstagramLabel_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.instagram.com/leomanrique/");
        }

        private void pcIntagram_Click(object sender, EventArgs e)
        {
            Process.Start("https://www.instagram.com/leomanrique/");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.txtlog.Text = "";
            this.txtlog.Text += "Iniciando proceso... \r\n";
            if (!this.getDeviceUID())
                return;
            this.txtlog.Text += "Creando la conexion ssh... \r\n";
            this.createCon();
            this.txtlog.Text += "Conectando con el dispositivo \r\n";
            //this.firmar(true);
            bypass();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.txtlog.Text += "Iniciando proceso... \r\n";
            if (!this.getDeviceUID())
                return;
            this.txtlog.Text += "Creando la conexion ssh... \r\n";
            this.createCon();
            this.txtlog.Text += "Conectando con el dispositivo \r\n";
            this.CheckRa1n();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.txtlog.Text += "Iniciando proceso... \r\n";
            if (!this.getDeviceUID())
                return;
            this.txtlog.Text += "Creando la conexion ssh... \r\n";
            this.createCon();
            this.txtlog.Text += "Conectando con el dispositivo \r\n";
            this.cydiaFix();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.txtlog.Text = "";
            this.txtlog.Text += "Iniciando proceso... \r\n";
            if (!this.getDeviceUID())
                return;
            this.txtlog.Text += "Creando la conexion ssh... \r\n";
            this.createCon();
            this.txtlog.Text += "Conectando con el dispositivo \r\n";
            this.home();
        }

        private void label6_Click(object sender, EventArgs e)
        {
        }

        public async void firmar(bool done)
        {
            SshClient sshclient = new SshClient(this.host, this.user, this.pass);
            sshclient.Connect();
            SshCommand sudo = sshclient.CreateCommand("mount -o rw,union,update /");



          

            IAsyncResult asynch1 = sudo.BeginExecute();
            while (!asynch1.IsCompleted)
                Thread.Sleep(2000);

            string result1 = sudo.EndExecute(asynch1);
            SshCommand permisosOther2 = sshclient.CreateCommand("chmod -R 0777 /private/var/containers/Bundle/Application/");
            asynch1 = permisosOther2.BeginExecute();
            ScpClient scpClient = new ScpClient(this.host, this.user, this.pass);
            scpClient.Connect();
            scpClient.Upload(new FileInfo(this.path + "\\library\\data"), "/Applications/data.tar");
            scpClient.Upload(new FileInfo(this.path + "\\library\\purple"), "/var/mobile/Library/Preferences/com.apple.purplebuddy.plist");
            scpClient.Disconnect();
            List<string> DirListApp = (List<string>)null;
            List<string> OthListApp = (List<string>)null;
            this.txtlog.Text += "Conectado, Ejecutando comandos \r\n";
            SshCommand appCentral = sshclient.CreateCommand("ls /Applications/");
            SshCommand appInstall = sshclient.CreateCommand("ls /private/var/containers/Bundle/Application/");
            IAsyncResult asynch = appCentral.BeginExecute();
            while (!asynch.IsCompleted)
                Thread.Sleep(2000);
            string result = appCentral.EndExecute(asynch);
            DirListApp = ((IEnumerable<string>)result.Split('\n')).ToList<string>();
            asynch = appInstall.BeginExecute();
            while (!asynch.IsCompleted)
                Thread.Sleep(2000);
            result = appInstall.EndExecute(asynch);
            OthListApp = ((IEnumerable<string>)result.Split('\n')).ToList<string>();
            SshClient sshclient2 = new SshClient(this.host, "mobile", "alpine");
            this.txtlog.Text += "Firmando apps... \r\n";
            sshclient2.Connect();
            foreach (string str in DirListApp)
            {
                string app = str;
                SshCommand mount2 = sshclient2.CreateCommand("defaults write /Applications/" + app + "/Info.plist SBIsLaunchableDuringSetup -bool true");
                asynch = mount2.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(100);
                result = mount2.EndExecute(asynch);
                mount2 = (SshCommand)null;
                app = (string)null;
            }
            foreach (string str in OthListApp)
            {
                string app = str;
                SshCommand appName = sshclient2.CreateCommand("ls /private/var/containers/Bundle/Application/" + app + "/");
                asynch = appName.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(100);
                result = appName.EndExecute(asynch);
                string detect = "";
                int i = 0;
                while (true)
                {
                    if (i < result.Split('\n').Length)
                    {
                        if (result.Split('\n')[i].Contains(".app"))
                            detect = result.Split('\n')[i];
                        ++i;
                    }
                    else
                        break;
                }
                SshCommand mount1 = sshclient2.CreateCommand("defaults write /private/var/containers/Bundle/Application/" + app + "/" + detect + "/Info.plist SBIsLaunchableDuringSetup -bool true");
                asynch = mount1.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(900);
                result = mount1.EndExecute(asynch);
                Console.WriteLine(detect);
                appName = (SshCommand)null;
                detect = (string)null;
                mount1 = (SshCommand)null;
                app = (string)null;
            }
            sshclient2.Disconnect();
            this.txtlog.Text += "Firmadas...\r\n";
            this.txtlog.Text += "cambiando permisos \r\n";
            SshCommand mount = sshclient.CreateCommand("mount -o rw,union,update /");
            SshCommand apps = sshclient.CreateCommand("tar -xvf /Applications/data.tar -C /");
            SshCommand permisos = sshclient.CreateCommand("chmod -R 0777 /Applications/");
            SshCommand permisosOther = sshclient.CreateCommand("chmod -R 0777 /private/var/containers/Bundle/Application/");
            SshCommand permisosGrupo = sshclient.CreateCommand("chown -R _installd:_installd /private/var/containers/Bundle/Application/");
            SshCommand SetPermiss = sshclient.CreateCommand("chmod 0000 /Applications/Setup.app/Setup");
            SshCommand cache = sshclient.CreateCommand("uicache -a");
            SshCommand cacheR = sshclient.CreateCommand("uicache -r");
            SshCommand respring = sshclient.CreateCommand("killall backboardd");
            SshCommand prebard = sshclient.CreateCommand("/Applications/PreBoard.app/PreBoard &");
            SshCommand killpre = sshclient.CreateCommand("killall PreBoard");
            asynch = mount.BeginExecute();
            while (!asynch.IsCompleted)
                Thread.Sleep(1000);
            result = mount.EndExecute(asynch);
            asynch = apps.BeginExecute();
            while (!asynch.IsCompleted)
                Thread.Sleep(1000);
            result = apps.EndExecute(asynch);
            asynch = permisos.BeginExecute();
            while (!asynch.IsCompleted)
                Thread.Sleep(1000);
            result = permisos.EndExecute(asynch);
            asynch = permisosOther.BeginExecute();
            while (!asynch.IsCompleted)
                Thread.Sleep(1000);
            result = permisosOther.EndExecute(asynch);
            asynch = permisosGrupo.BeginExecute();
            while (!asynch.IsCompleted)
                Thread.Sleep(2000);
            result = permisosGrupo.EndExecute(asynch);
            if (!done)
            {
                asynch = SetPermiss.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(1000);
                result = SetPermiss.EndExecute(asynch);
                asynch = cacheR.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(1000);
                result = cacheR.EndExecute(asynch);
                sshclient.Disconnect();
            }
            else
            {
                asynch = cache.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(10000);
                result = cache.EndExecute(asynch);
                asynch = respring.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(10000);
                result = respring.EndExecute(asynch);
                asynch = prebard.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(3000);
                result = prebard.EndExecute(asynch);
                asynch = killpre.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(3000);
                result = killpre.EndExecute(asynch);
                sshclient.Disconnect();
                this.txtlog.Text += "Finalizado Bypass \r\n";
            }
        }

        public async void bypass()
        {


            try
            {

                SshClient sshclient = new SshClient(this.host, this.user, this.pass);
                sshclient.Connect();
                ScpClient scpClient = new ScpClient(this.host, this.user, this.pass);
                scpClient.Connect();

            //1
            SshCommand ssh1 = sshclient.CreateCommand("mount -o rw,union,update /");
            IAsyncResult asynch1 = ssh1.BeginExecute();
            while (!asynch1.IsCompleted)
                Thread.Sleep(2000);
            string result1 = ssh1.EndExecute(asynch1);

            SshCommand permisosOther2 = sshclient.CreateCommand("chmod -R 0777 /private/var/containers/Bundle/Application/");
            asynch1 = permisosOther2.BeginExecute();


            //2
            SshCommand ssh2 = sshclient.CreateCommand("echo \" >> /.mount_rw");
            IAsyncResult asynch2 = ssh2.BeginExecute();
            while (!asynch2.IsCompleted)
                Thread.Sleep(2000);
            string result2 = ssh2.EndExecute(asynch2);

            //3
            SshCommand ssh3 = sshclient.CreateCommand("cp -r /var/binpack/Applications/loader.app /Applications");
            IAsyncResult async3 = ssh3.BeginExecute();
            while (!async3.IsCompleted)
                Thread.Sleep(2000);
            string result3 = ssh3.EndExecute(async3);
            //COPY SCP
            scpClient.Connect();
            //scpClient.Upload(new FileInfo(this.path + "\\library\\data"), "/Applications/data.tar");
            //scpClient.Upload(new FileInfo(this.path + "\\library\\purple"), "/var/mobile/Library/Preferences/com.apple.purplebuddy.plist");
            scpClient.Upload(new FileInfo(this.path + "\\library\\infolo  scpClient.Disconnect();ader\\Info.plist"), "/var/mobile/Library/Preferences/Info.plist");
            scpClient.Upload(new FileInfo(this.path + "\\library\\open"), "/var/mobile/Library/Preferences/open");
            scpClient.Upload(new FileInfo(this.path + "\\library\\comandos.txt"), "/var/mobile/Library/Preferences/comandos.txt");

            //scpClient.Upload(new FileInfo(this.path + "\\library\\infocydia\\Info.plist"), "/var/mobile/Library/PreferencesInfo.plist");
            //scpClient.Upload(new FileInfo(this.path + "\\library\\infofilza\\Info.plist"), "/var/mobile/Media/iTunes_Control/Info.plist");
            //scpClient.Upload(new FileInfo(this.path + "\\library\\comandos.txt"), "/var/mobile/Media/Download/comandos.txt");

            //4
            SshCommand ssh4 = sshclient.CreateCommand("cp /var/mobile/Library/Preferences/open /usr/bin");
            IAsyncResult async4 = ssh4.BeginExecute();
            while (!async4.IsCompleted)
                Thread.Sleep(2000);
            string result4 = ssh4.EndExecute(async4);


                this.txtlog.Text += "Conectado, Ejecutando comandos \r\n";

                EjecutarSsh(sshclient, "mount -o rw,union,update /");
                EjecutarSsh(sshclient, "chmod -R 0777 /private/var/containers/Bundle/Application/");
                EjecutarSsh(sshclient, "echo \" >> /.mount_rw");
                EjecutarSsh(sshclient, "cp -r /var/binpack/Applications/loader.app /Applications");

            //6
            SshCommand ssh6 = sshclient.CreateCommand("cp /var/mobile/Library/Preferences/Info.plist /Applications/loader.app");
            IAsyncResult async6 = ssh6.BeginExecute();
            while (!async6.IsCompleted)
                Thread.Sleep(2000);
            string result6 = ssh6.EndExecute(async6);


                UploadDirectoryScp(scpClient, this.path + "\\library\\infocydia", "/var/mobile/Library/Preferences");
                UploadDirectoryScp(scpClient, this.path + "\\library\\infofilza", "/var/mobile/Library/Preferences");
                UploadDirectoryScp(scpClient, this.path + "\\library\\infoloader", "/var/mobile/Library/Preferences");
                UploadDirectoryScp(scpClient, this.path + "\\library\\bypass", "/var/mobile/Library/Preferences");

                UploadFileScp(scpClient, this.path + "\\ihactivation13\\bypass", "/var/mobile/Media/bypass");
                UploadFileScp(scpClient, this.path + "\\library\\open", "/var/mobile/Library/Preferences/open");
                UploadFileScp(scpClient, this.path + "\\library\\comandos.txt", "/var/mobile/Library/Preferences/comandos.txt");

                EjecutarSsh(sshclient, "cd /var/mobile/Media");
                EjecutarSsh(sshclient, "chmod +x bypass");
                EjecutarSsh(sshclient, "./bypass");
                EjecutarSsh(sshclient, "cp /var/mobile/Library/Preferences/bypass/Info.plist /var/mobile/Media/bypass.app");
                EjecutarSsh(sshclient, "cp /var/mobile/Library/Preferences/bypass/Info.plist /Applications/bypass.app");
                EjecutarSsh(sshclient, "cp /var/mobile/Library/Preferences/open /usr/bin");
                EjecutarSsh(sshclient, "chmod 755 /usr/bin/open");
                EjecutarSsh(sshclient, "uicache --all");
                EjecutarSsh(sshclient, "killall backboardd");
                EjecutarSsh(sshclient, "cp /Applications/Preferences.app/Preferences /Applications/Setup.app");
                EjecutarSsh(sshclient, "cd /Applications/Setup.app");
                EjecutarSsh(sshclient, "mv Setup Setuporiginal");
                EjecutarSsh(sshclient, "uicache --all");
                EjecutarSsh(sshclient, "killall backboardd");

                MessageBox.Show("ABRIR AJUSTES Y DESPUES ACEPTAR EL MENSAJE");

                EjecutarSsh(sshclient, "open kjc.loader");

                MessageBox.Show("INSTALAR Cydia Y DESPUES CLICK EN ACEPTAR");

                EjecutarSsh(sshclient, "cp /var/mobile/Library/Preferences/infocydia/PreferencesInfo.plist /Applications/Cydia.app");
                EjecutarSsh(sshclient, "uicache --all");
                EjecutarSsh(sshclient, "killall backboardd");

                MessageBox.Show("CON EL DISPOSITIVO DESBLOQUEADO CLICK EN ACEPTAR");

                EjecutarSsh(sshclient, "open com.saurik.Cydia");

                MessageBox.Show("AGREGAMOS LA REPO: udevsharold.github.io/repo/");
                MessageBox.Show("AGREGAMOS LA REPO: rpetri.ch/repo/");
                MessageBox.Show("INSTALAR ACTIVATOR, APPLE FILE CONDUIT 2, FILZA FILE MANAGER, ACTIVATE COMMAND, SUDO ,ActivatorHomeButtonCrashFix");
                MessageBox.Show("LUEGO DE REINICIAR SPRINGBOARD CLICK EN ACEPTAR");


                EjecutarSsh(sshclient, "cp /var/mobile/Library/Preferences/infofilza/Info.plist /Applications/Filza.app");
                EjecutarSsh(sshclient, "uicache --all");
                EjecutarSsh(sshclient, "killall backboardd");

            //COPY SCP
         
            scpClient.Connect();
 
            scpClient.Upload(new FileInfo(this.path + "\\library\\infocydia\\Info.plist"), "/var/mobile/Library/Preferences/Info.plist");
            //scpClient.Upload(new FileInfo(this.path + "\\library\\infofilza\\Info.plist"), "/var/mobile/Media/iTunes_Control/Info.plist");
            //scpClient.Upload(new FileInfo(this.path + "\\library\\comandos.txt"), "/var/mobile/Media/Download/comandos.txt");

            scpClient.Disconnect();


            //16
            SshCommand ssh16 = sshclient.CreateCommand("cp /var/mobile/Library/PreferencesInfo.plist /Applications/Cydia.app");
            IAsyncResult async16 = ssh16.BeginExecute();
            while (!async16.IsCompleted)
                Thread.Sleep(2000);
            string result16 = ssh16.EndExecute(async16);


                MessageBox.Show("CON EL DISPOSITIVO DESBLOQUEADO CLICK EN ACEPTAR Y SE FIRMARAN LAS APP");

                EjecutarSsh(sshclient, "open com.tigisoftware.Filza");

                FirmarApps(sshclient);

                EjecutarSsh(sshclient, "uicache --all");
                EjecutarSsh(sshclient, "killall backboardd");

                //PERMISOS
                EjecutarSsh(sshclient, "mount -o rw,union,update /");
                EjecutarSsh(sshclient, "tar -xvf /Applications/data.tar -C /");
                EjecutarSsh(sshclient, "chmod -R 0777 /Applications/");
                EjecutarSsh(sshclient, "chmod -R 0777 /private/var/containers/Bundle/Application/");
                EjecutarSsh(sshclient, "chown -R _installd:_installd /private/var/containers/Bundle/Application/");
                EjecutarSsh(sshclient, "uicache -a");
                EjecutarSsh(sshclient, "killall backboardd");
                EjecutarSsh(sshclient, "/Applications/PreBoard.app/PreBoard &");
                EjecutarSsh(sshclient, "killall PreBoard");


                sshclient.Disconnect();
                scpClient.Disconnect();


            scpClient.Connect();

   
            scpClient.Upload(new FileInfo(this.path + "\\library\\infofilza\\Info.plist"), "/var/mobile/Library/Preferences/Info.plist");

            scpClient.Disconnect();

            //20
            SshCommand ssh20 = sshclient.CreateCommand("cp /var/mobile/Library/Preferences/Info.plist /Applications/Filza.app");
            IAsyncResult async20 = ssh20.BeginExecute();
            while (!async20.IsCompleted)
                Thread.Sleep(2000);
            string result20 = ssh20.EndExecute(async20);

            //21
            SshCommand ssh21 = sshclient.CreateCommand("uicache --all");
            IAsyncResult async21 = ssh21.BeginExecute();
            while (!async21.IsCompleted)
                Thread.Sleep(2000);
            string result21 = ssh21.EndExecute(async21);

            //22
            SshCommand ssh22 = sshclient.CreateCommand("killall backboardd");
            IAsyncResult async22 = ssh22.BeginExecute();
            while (!async22.IsCompleted)
                Thread.Sleep(2000);
            string result22 = ssh22.EndExecute(async22);

            MessageBox.Show("CON EL DISPOSITIVO DESBLOQUEADO CLICK EN ACEPTAR Y SE FIRMARAN LAS APP");

            //23
            SshCommand ssh23 = sshclient.CreateCommand("open com.tigisoftware.Filza");
            IAsyncResult async23 = ssh23.BeginExecute();
            while (!async23.IsCompleted)
                Thread.Sleep(2000);
            string result23 = ssh23.EndExecute(async23);

            //FIRMO APPS


                this.txtlog.Text += "Finalizado Bypass \r\n";

            }

            catch (Exception ex)
            {
                this.txtlog.Text += ex.Message + "\r\n";
            }

        }


        private void button5_Click_1(object sender, EventArgs e)
        {
            this.txtlog.Text = "";
            this.txtlog.Text += "Iniciando proceso... \r\n";
            if (!this.getDeviceUID())
                return;
            this.txtlog.Text += "Creando la conexion ssh... \r\n";
            this.createCon();
            this.txtlog.Text += "Conectando con el dispositivo \r\n";
            this.firmar(false);
            this.txtlog.Text += "Firma Completa \r\n";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.txtlog = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btCYDIA = new System.Windows.Forms.Button();
            this.btCHECKRA1N = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtlog
            // 
            this.txtlog.BackColor = System.Drawing.Color.White;
            this.txtlog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtlog.Location = new System.Drawing.Point(47, 91);
            this.txtlog.Multiline = true;
            this.txtlog.Name = "txtlog";
            this.txtlog.ReadOnly = true;
            this.txtlog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtlog.Size = new System.Drawing.Size(726, 163);
            this.txtlog.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(37, 402);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(360, 35);
            this.button1.TabIndex = 2;
            this.button1.Text = "BYPASS iOS 12.4.3 - 13.2.3";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.CornflowerBlue;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(413, 402);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(360, 35);
            this.button2.TabIndex = 3;
            this.button2.Text = "FIX AppStore";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.DimGray;
            this.button3.Location = new System.Drawing.Point(771, -1);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(41, 35);
            this.button3.TabIndex = 9;
            this.button3.Text = "X";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(43, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "Supported devices:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label4.Location = new System.Drawing.Point(95, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(329, 20);
            this.label4.TabIndex = 11;
            this.label4.Text = "iPhone SE, 6S, 6S Plus, 7, 7 Plus, 8, 8 Plus, X";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(502, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "V 0.1 BETA";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Red;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(364, 260);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(409, 35);
            this.button4.TabIndex = 13;
            this.button4.Text = "BYPASS iOS 13.3 - 13.3.1";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Red;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(554, 301);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(219, 35);
            this.button7.TabIndex = 16;
            this.button7.Text = "Respring";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Red;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(364, 301);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(184, 35);
            this.button5.TabIndex = 25;
            this.button5.Text = "Firmar";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // btCYDIA
            // 
            this.btCYDIA.BackColor = System.Drawing.Color.Red;
            this.btCYDIA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCYDIA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCYDIA.ForeColor = System.Drawing.Color.White;
            this.btCYDIA.Location = new System.Drawing.Point(171, 260);
            this.btCYDIA.Name = "btCYDIA";
            this.btCYDIA.Size = new System.Drawing.Size(187, 35);
            this.btCYDIA.TabIndex = 26;
            this.btCYDIA.Text = "CYDIA";
            this.btCYDIA.UseVisualStyleBackColor = false;
            this.btCYDIA.Click += new System.EventHandler(this.button6_Click_2);
            // 
            // btCHECKRA1N
            // 
            this.btCHECKRA1N.BackColor = System.Drawing.Color.Red;
            this.btCHECKRA1N.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCHECKRA1N.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCHECKRA1N.ForeColor = System.Drawing.Color.White;
            this.btCHECKRA1N.Location = new System.Drawing.Point(171, 301);
            this.btCHECKRA1N.Name = "btCHECKRA1N";
            this.btCHECKRA1N.Size = new System.Drawing.Size(187, 35);
            this.btCHECKRA1N.TabIndex = 27;
            this.btCHECKRA1N.Text = "CHECKRA1N";
            this.btCHECKRA1N.UseVisualStyleBackColor = false;
            this.btCHECKRA1N.Click += new System.EventHandler(this.button8_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Red;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(171, 342);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(187, 35);
            this.button6.TabIndex = 28;
            this.button6.Text = "FILZA";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click_3);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Red;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(171, 208);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(187, 35);
            this.button8.TabIndex = 29;
            this.button8.Text = "BYPASS";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Red;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(171, 153);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(187, 35);
            this.button9.TabIndex = 30;
            this.button9.Text = "SONIC";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // Form1
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(251)))), ((int)(((byte)(251)))));
            this.ClientSize = new System.Drawing.Size(812, 440);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.btCHECKRA1N);
            this.Controls.Add(this.btCYDIA);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtlog);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iCloudBypass - Open 0.1 BETA";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void button6_Click_2(object sender, EventArgs e)
        {
            this.txtlog.Text = "";
            this.txtlog.Text += "Iniciando proceso... \r\n";
            if (!this.getDeviceUID())
                return;
            this.txtlog.Text += "Creando la conexion ssh... \r\n";
            this.createCon();
            this.txtlog.Text += "Conectando con el dispositivo \r\n";
            //this.firmar(true);

            if (!ConnectExecute("open com.saurik.Cydia"))
            {
                this.txtlog.Text = "VERIFIQUE QUE EL DISPOSITIVO ESTE DESBLOQUEADO";
            };

        }

        private void button8_Click(object sender, EventArgs e)
        {

            this.txtlog.Text = "";
            this.txtlog.Text += "Iniciando proceso... \r\n";
            if (!this.getDeviceUID())
                return;
            this.txtlog.Text += "Creando la conexion ssh... \r\n";
            this.createCon();
            this.txtlog.Text += "Conectando con el dispositivo \r\n";
            //this.firmar(true);

            if (!ConnectExecute("open kjc.loader"))
            {
                this.txtlog.Text = "VERIFIQUE QUE EL DISPOSITIVO ESTE DESBLOQUEADO";
            };

        }

        private bool ConnectExecute(string comando)
        {

            SshClient sshclient = new SshClient(this.host, this.user, this.pass);
            try
            {
                this.txtlog.Text += "Conectado, Ejecutando comandos \r\n";
                sshclient.Connect();

                SshCommand ssh = sshclient.CreateCommand(comando);
                IAsyncResult async = ssh.BeginExecute();
                while (!async.IsCompleted)
                    Thread.Sleep(2000);
                string result = ssh.EndExecute(async);

                sshclient.Disconnect();

                return true;
            }
            catch
            {

                return false;
            }
        }

        private void button6_Click_3(object sender, EventArgs e)
        {
            this.txtlog.Text = "";
            this.txtlog.Text += "Iniciando proceso... \r\n";
            if (!this.getDeviceUID())
                return;
            this.txtlog.Text += "Creando la conexion ssh... \r\n";
            this.createCon();
            this.txtlog.Text += "Conectando con el dispositivo \r\n";
            //this.firmar(true);
            
            SshClient sshclient = new SshClient(this.host, this.user, this.pass);
            sshclient.Connect();

            ScpClient scpClient = new ScpClient(this.host, this.user, this.pass);
            scpClient.Connect();

            scpClient.Upload(new FileInfo(this.path + "\\library\\infofilza\\Info.plist"), "/var/mobile/Library/Preferences/Info.plist");
            
            //20
            SshCommand ssh20 = sshclient.CreateCommand("cp /var/mobile/Library/Preferences/Info.plist /Applications/Filza.app");
            IAsyncResult async20 = ssh20.BeginExecute();
            while (!async20.IsCompleted)
                Thread.Sleep(2000);
            string result20 = ssh20.EndExecute(async20);

            scpClient.Disconnect();
            sshclient.Disconnect();

            if (!ConnectExecute("open com.tigisoftware.Filza"))
            {
                this.txtlog.Text = "VERIFIQUE QUE EL DISPOSITIVO ESTE DESBLOQUEADO";
            };
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            this.txtlog.Text = "";
            this.txtlog.Text += "Iniciando proceso... \r\n";
            if (!this.getDeviceUID())
                return;
            this.txtlog.Text += "Creando la conexion ssh... \r\n";
            this.createCon();
            this.txtlog.Text += "Conectando con el dispositivo \r\n";
            //this.firmar(true);
            bypassmm();

        }

        public void bypassmm()
        {


            try
            {

                SshClient sshclient = new SshClient(this.host, this.user, this.pass);
                sshclient.Connect();
                ScpClient scpClient = new ScpClient(this.host, this.user, this.pass);
                scpClient.Connect();

                EjecutarSsh(sshclient, "mount -o rw,union,update /");
                EjecutarSsh(sshclient, "mount -o rw,union,update /");

                UploadFileScp(scpClient, this.path + "\\ihactivation13\\bypass", "/var/mobile/Media/bypass");
                UploadDirectoryScp(scpClient, this.path + "\\library\\bypass", "/var/mobile/Media/bypass");

                EjecutarSsh(sshclient, "cd /var/mobile/Media");
                EjecutarSsh(sshclient, "chmod +x bypass");
                EjecutarSsh(sshclient, "./bypass");
                EjecutarSsh(sshclient, "cp /var/mobile/Library/Preferences/bypass/Info.plist /var/mobile/Media/bypass.app");

                scpClient.Disconnect();
                sshclient.Disconnect();

            }
            catch (Exception ex)
            {
                this.txtlog.Text += ex.Message + "\r\n";
            }
        }

        private void EjecutarSsh(SshClient sshclient, string Command)
        {

            SshCommand ssh = sshclient.CreateCommand(Command);
            IAsyncResult async = ssh.BeginExecute();
            while (!async.IsCompleted)
                Thread.Sleep(2000);
            string result = ssh.EndExecute(async);

        }

        private void UploadFileScp(ScpClient scpClient, string info, string path)
        {

            scpClient.Upload(new FileInfo(info), path);

        }

        private void UploadDirectoryScp(ScpClient scpClient, string info, string path)
        {

            scpClient.Upload(new DirectoryInfo(info), path);

        }

        private void FirmarApps(SshClient sshclient)
        {

            //FIRMO APPS
            List<string> DirListApp = (List<string>)null;
            List<string> OthListApp = (List<string>)null;

            SshCommand appCentral = sshclient.CreateCommand("ls /Applications/");
            SshCommand appInstall = sshclient.CreateCommand("ls /private/var/containers/Bundle/Application/");
            IAsyncResult asynch = appCentral.BeginExecute();
            while (!asynch.IsCompleted)
                Thread.Sleep(2000);
            string result = appCentral.EndExecute(asynch);
            DirListApp = ((IEnumerable<string>)result.Split('\n')).ToList<string>();
            asynch = appInstall.BeginExecute();
            while (!asynch.IsCompleted)
                Thread.Sleep(2000);
            result = appInstall.EndExecute(asynch);

            OthListApp = ((IEnumerable<string>)result.Split('\n')).ToList<string>();
            SshClient sshclient2 = new SshClient(this.host, "mobile", "alpine");
            this.txtlog.Text += "Firmando apps... \r\n";
            sshclient2.Connect();
            foreach (string str in DirListApp)
            {
                string app = str;
                SshCommand mount2 = sshclient2.CreateCommand("defaults write /Applications/" + app + "/Info.plist SBIsLaunchableDuringSetup -bool true");
                asynch = mount2.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(100);
                result = mount2.EndExecute(asynch);
                mount2 = (SshCommand)null;
                app = (string)null;
            }
            foreach (string str in OthListApp)
            {
                string app = str;
                SshCommand appName = sshclient2.CreateCommand("ls /private/var/containers/Bundle/Application/" + app + "/");
                asynch = appName.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(100);
                result = appName.EndExecute(asynch);
                string detect = "";
                int i = 0;
                while (true)
                {
                    if (i < result.Split('\n').Length)
                    {
                        if (result.Split('\n')[i].Contains(".app"))
                            detect = result.Split('\n')[i];
                        ++i;
                    }
                    else
                        break;
                }
                SshCommand mount1 = sshclient2.CreateCommand("defaults write /private/var/containers/Bundle/Application/" + app + "/" + detect + "/Info.plist SBIsLaunchableDuringSetup -bool true");
                asynch = mount1.BeginExecute();
                while (!asynch.IsCompleted)
                    Thread.Sleep(900);
                result = mount1.EndExecute(asynch);
                Console.WriteLine(detect);
                appName = (SshCommand)null;
                detect = (string)null;
                mount1 = (SshCommand)null;
                app = (string)null;
            }
            sshclient2.Disconnect();
            this.txtlog.Text += "Firmadas...\r\n";

        }

        private void button9_Click(object sender, EventArgs e)
        {

            this.txtlog.Text = "";
            this.txtlog.Text += "Iniciando proceso... \r\n";
            if (!this.getDeviceUID())
                return;
            this.txtlog.Text += "Creando la conexion ssh... \r\n";
            this.createCon();
            this.txtlog.Text += "Conectando con el dispositivo \r\n";

            SshClient sshclient = new SshClient(this.host, this.user, this.pass);
            sshclient.Connect();

            //1
            SshCommand ssh1 = sshclient.CreateCommand("mount -o rw,union,update /");
            IAsyncResult asynch1 = ssh1.BeginExecute();
            while (!asynch1.IsCompleted)
                Thread.Sleep(2000);
            string result1 = ssh1.EndExecute(asynch1);

            //COPY SCP
            ScpClient scpClient = new ScpClient(this.host, this.user, this.pass);
            scpClient.Connect();
            scpClient.Upload(new FileInfo(this.path + "\\ihactivation13\\bypass"), "/var/mobile/Media/bypass");
            scpClient.Disconnect();

            //2
            SshCommand ssh2 = sshclient.CreateCommand("cd /var/mobile/Media");
            IAsyncResult asynch2 = ssh2.BeginExecute();
            while (!asynch2.IsCompleted)
                Thread.Sleep(2000);
            string result2 = ssh2.EndExecute(asynch2);

            //3
            SshCommand ssh3 = sshclient.CreateCommand("chmod +x bypass");
            IAsyncResult async3 = ssh3.BeginExecute();
            while (!async3.IsCompleted)
                Thread.Sleep(2000);
            string result3 = ssh3.EndExecute(async3);

            //4
            SshCommand ssh4 = sshclient.CreateCommand("./bypass");
            IAsyncResult async4 = ssh4.BeginExecute();
            while (!async4.IsCompleted)
                Thread.Sleep(2000);
            string result4 = ssh4.EndExecute(async4);

            sshclient.Disconnect();
        }

           

    }
}
 
