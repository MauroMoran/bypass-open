﻿using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace iCloudBypass_ShiftKey.Properties
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (iCloudBypass_ShiftKey.Properties.Resources.resourceMan == null)
          iCloudBypass_ShiftKey.Properties.Resources.resourceMan = new ResourceManager("iCloudBypass_ShiftKey.Properties.Resources", typeof (iCloudBypass_ShiftKey.Properties.Resources).Assembly);
        return iCloudBypass_ShiftKey.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return iCloudBypass_ShiftKey.Properties.Resources.resourceCulture;
      }
      set
      {
        iCloudBypass_ShiftKey.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
